/*jslint node: true */
'use strict';
/**
 * Created by sixtu on 01/07/2016.
 */


//var enviroment = process.env.APP_ENV || 'undefined'; //Set enviroment
var redisUrl = "pub-redis-14552.us-east-1-3.6.ec2.redislabs.com:14552";
var config = require('./modules/config-loader')(process.env.APP_ENV); //APP_ENV is a custom

var enviroment = process.env.APP_ENV || 'undefined';
var port = process.env.PORT || 5000; //Set default port
var shortid = require('shortid');
var phantom = require('phantom');
var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server),
    fs = require('fs'),
    nunjucks = require('nunjucks'),
    marko = require('marko'),
    bcrypt = require('bcrypt-nodejs'),
    trim = require('trim'),
    multer = require('multer'),
    moment = require('moment'),
    slug = require('slug'),
     randomstring = require("randomstring"),
    methodOverride = require('method-override'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'), //for processing post body
    morgan = require('morgan'), //Loging req info in dev
    async = require('async'),
    stripe = require("stripe")(config.STRIPE_SECRET_KEY),
    path = require('path'),
    redis = require("redis"),
    bcrypt = require('bcrypt-nodejs'),
    promise = require('bluebird'),
    session = require('express-session'), //Sessions
    pg = require('pg'),
    pgSession = require('connect-pg-simple')(session),
    KnexSessionStore = require('connect-session-knex')(session),
    cloudinary  = require('cloudinary');

    cloudinary.config({
    cloud_name: config.CLOUDINARY_CLOUD_NAME,
    api_key: config.CLOUDINARY_API_KEY,
    api_secret: config.CLOUDINARY_API_SECRET
});

//Multer setup
var storage        = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
       // cb(null, file.fieldname + '-' + Date.now())
        cb(null, Math.random().toString(36) + path.extname(file.originalname));
    }
});
var upload         = multer({ storage: storage }).array('image',2);
/*
var cloudinary  = require('cloudinary');
cloudinary.config({
    cloud_name: config.CLOUDINARY_CLOUD_NAME,
    api_key: config.CLOUDINARY_API_KEY,
    api_secret: config.CLOUDINARY_API_SECRET
});
*/

//DB Setup
var config = {
  user: 'postgres', //env var: PGUSER
  database: 'flibuy_development', //env var: PGDATABASE
  password: 'root', //env var: PGPASSWORD
  port: 5432, //env var: PGPORT
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};

var pool = new pg.Pool(config);

 /*
// PG conecction options
var options = {
  // Initialization Options
  promiseLib: promise
};
var pgp = require('pg-promise')(options);
var connectionString = 'postgres://localhost:5432/flibuy_development';
var db = pgp({
    host: '127.0.0.1',
    port: 5433,
    database: 'flibuy_development',
    user: 'postgres',
    password: 'root'
             });
*/
var knex = require('./database');

//models
var User = require('./models/user');
    //console.log( rtg);



/*
//Cloudinary
cloudinary.config({
    cloud_name: config.CLOUDINARY_CLOUD_NAME,
    api_key: config.CLOUDINARY_API_KEY,
    api_secret: config.CLOUDINARY_API_SECRET
});
*/
/*
//Configure marko template
app.engine ('marko', function(filePath, options, callback) {
    console.log(filePath);
    marko.load(filePath).render(options, function(err, output) {
        callback(null, output);
    });
});
app.set('views', './views');
app.set('view engine', 'marko');
*/


        //App configurations
        if (enviroment === 'undefined' || enviroment === 'development') {
            app.use(morgan('dev'));
        }
        //use req.ip or req.ips to get the ips
        //app.enable('trust proxy'); //USe only if the server is behind a proxy server
        app.use(methodOverride());
        app.use(cookieParser());
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(express.static(path.join(__dirname, 'public'))); //serve static files
        app.use(express.static(path.join(__dirname, '/assets'))); //serve static files
        app.use(express.static(path.join(__dirname, 'fonts'))); //serve static files
        app.use(express.static(path.join(__dirname, '/bower_components'))); //serve static files

        //Configur express session using a redis data store
        var MemoryStore = session.MemoryStore;


var store = new KnexSessionStore({
                    knex: knex,
                    tablename: 'sessions' // optional. Defaults to 'sessions'
                });

/*
       var store =  new pgSession({
            pg : pg,                                  // Use global pg-module
            conString : 'postgres://postgres:root@localhost:5432/flibuy_development', // Connect using something else than default DATABASE_URL env variable
            tableName : 'sessions'               // Use another table-name than the default "session" one
          });
*/
        app.use(session({
            secret: 'secret',
            saveUninitialized: false,
            resave: false,
            store: store
            /* store: new RedisStore({host: rtg.prtotocol, port: rtg.port, client: client})*/
        }));


        //Set view engine
        app.set('views', 'views');
        app.set('view engine', 'html');

        //Configue nunjucks template engine
        nunjucks.configure('views', {
          autoescape: true,
          express   : app
        });

  //Socket.io conecction
    io.on('connection', function(socket){
       console.log('A Client has connected to the web socket');
        //socket.emit("message",["Hi", "There"]);
    });

    //Global settings
    //app.locals.basedir          = path.join(__dirname, 'views');
    moment.locale('es'); //Set language to spanish
    app.locals.moment = moment;
    app.locals.cloudinary = cloudinary;



//Check if user has session
 app.use(function(req, res, next){
        var user = {};
     if (req.session && req.session.user) {

       var query =  knex.from('users')
         .where({'id': req.session.user.id})
         .first();
         return query.then(function(results){
                 user.id            = results.id;
                 user.username      = results.username;
                 user.email         = results.email;
                 user.is_seller     = results.is_seller;
                 req.session.user   = user;
                 req.user           = user;
                 res.locals.user    = user; //makes the user object avialable in view
                 //console.log(req.session.user );
               return next();
         })
         .catch(function(err){
             if(req.session.user.is_seller === true){
                 return res.redirect('/login-business');
             }else{
                 return  res.redirect('/login-customer');
             }

             //req.session.destroy();
             return next(err);
         });
     }else{
        return next();
     }
 });

 /*
//STORE OR SHOP
var shop = require('./routes/shop/index')(app);

app.use('/shop',shop.home);


// Seller Routes
app.use(require('./routes/business/signup'));
app.use(require('./routes/business/login'));
app.use(require('./routes/business/dashboard'));
app.use(require('./routes/business/product/index'));
app.use(require('./routes/business/product_auctions/index'));
app.use(require('./routes/business/product_images/index'));


//Customer Routes
app.use(require('./routes/customer/signup/index'));
app.use(require('./routes/customer/login/index'));
app.use(require('./routes/customer/logout'));
app.use(require('./routes/customer/dashboard'));
//app.use(require('./routes/customer/bids'));
*/

/* START: SHOP ROUTES */
app.get('/', function(req, res) {
    return res.render('home/index', {
        pageTitle: 'Proximo lanzamiento'
    });
});

app.get('/shop', function(req, res) {

  pool.query('SELECT *,product_auctions.id AS auction_id  FROM products INNER JOIN product_auctions ON products.id = product_auctions.product_id   WHERE  product_auctions.visible_in_store = TRUE ')
  .then(function(products){
       //console.log(products.rows[0]);
      return res.render('home/store', {
                pageTitle: 'Inicio',
                     products: products.rows,
                });
  })
  .catch(function(error){
        console.log(error);
       return res.json({error: error.message});

    });

});

 app.get('/shop/products/show/:slug', function(req, res) {
     //Get single product by ID or BY UNIQUE SLUG
     //console.log(req.params.slug);
     //console.log(req.query.id);

     pool.query('SELECT *, pa.id as auction_id, ba.business_name  FROM products  INNER JOIN product_auctions pa ON products.id = pa.product_id INNER JOIN business_addresses ba ON pa.pickup_address = ba.id WHERE  pa.visible_in_store = TRUE  AND products.slug = $1', [req.params.slug])
      .then(function(products){
       //console.log(products.rows);
       var product = products.rows[0];

      pool.query('SELECT *  FROM product_questions WHERE product_id = $1 AND response IS NOT NULL ',[product.id])
     .then(function(results){
  //console.log(results.rows);
  var questions = results.rows;

  return res.render('home/product-details', {
    pageTitle:product.slug,
         product: product,
         questions: questions
    });
  })
  .catch(function(error){
      //console.log(error);
     return res.json({error: error.message});
});
  })
  .catch(function(error){
        //console.log(error);
       return res.json({error: error.message});
    });
});
/* END: SHOP ROUTES */

/* START: CUSTOMER GENERATE PURCHASE CONFIRMATION ROUTES */
app.get('/generate-reciept/:bid_id', function(req, res){

//TODO insert Shorid into purchase confirmation table tabel
  pool.query('SELECT  pb.*, pa.*, p.*,ba.* FROM product_bids pb INNER JOIN product_auctions pa ON pa.id = pb.product_auctions_id  INNER JOIN products p ON p.id = pb.product_id INNER JOIN business_addresses ba  ON ba.id = pa.pickup_address WHERE  pb.id = $1', [req.params.bid_id])
  .then(function(results){
      //console.log(results.rows[0]);
      //console.log(shortid.generate());
       res.render('customer/products/purchase-confirmation',{product: results.rows[0]});
/*
       var fullUrl = req.protocol + '://' + req.get('Host') + req.originalUrl;
       console.log(fullUrl);
      phantom.create().then(function(ph) {
      ph.createPage().then(function(page) {
          page.open(fullUrl).then(function(status) {
              page.render('confirmatio.pdf').then(function() {
                  console.log('Page Rendered');
                  ph.exit();
                  res.type('application/pdf');
                  res.end('confirmatio.pdf', 'binary');

              });
        });
    });
});

*/
  })
  .catch(function(err){
    console.log(err.message);
  });


});

/* START: CUSTOMER GENERATE PURCHASE CONFIRMATION  ROUTES */


/* START: BUSINESS  ADDRESS ROUTES */

app.post('/business-address/update', function(req, res){
  pool.query('UPDATE  business_addresses SET line_one = $1, city = $2,  post_code = $3, state = $4, country = $5, other_details = $6, business_name = $7, updated_at = $8 ,address_type = $9 WHERE  id = $10', [req.body.street, req.body.city, req.body.post_code, req.body.province, req.body.country, req.body.other_details, req.body.name, new Date(), req.body.address_type,req.body.address_id])
  .then(function(results){
      console.log(results);
      return res.json({message:'Dirección actualizada', redirectTo:'/business-address'});
  })
  .catch(function(err){
    console.log(err.message);
  });

});

app.get('/business-address/edit/:id', function(req, res){
  pool.query('SELECT * FROM business_addresses WHERE id = $1',[req.params.id])
  .then(function(results){
    console.log(results.rows);
      return res.render('business/address/edit',{address: results.rows[0]});
  })
  .catch(function(err){
     console.log(err.message);
  });

});

app.post('/business-address/create', function(req, res){
  console.log(req.body);

  if(!req.session ){
   return res.json({'error': 'Debe iniciar sesión para continuar'});
  }
   pool.query('INSERT INTO business_addresses (seller_id,line_one,city,post_code,state,country,other_details,created_at,business_name, address_type) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) ',[req.session.user.id,req.body.street,req.body.city,req.body.post_code,req.body.province,req.body.country,req.body.other_details, new Date(), req.body.name,req.body.address_type])
  .then(function(results){
      // console.log(results);
        return res.json({'message': 'Dirección guardada', redirectTo: '/business-address'});
   })
   .catch(function(err){
      // console.log(err);
        return res.json({'error': err.message});
   });

  return res.json({message:'OK', redirectTo:'/business-address'});
});

app.get('/business-address', function(req, res){
  pool.query('SELECT * FROM business_addresses WHERE seller_id = $1',[req.session.user.id])
  .then(function(results){
    //console.log(results.rows);
      return res.render('business/address/index',{addresses: results.rows});
  })
  .catch(function(err){
    console.log(err.message);
  });

});

app.get('/business-address/create', function(req, res){
  return res.render('business/address/create');
});


/* END: BUSINESS  ADDRESS ROUTES */





/* START: BUSINESS  PRODUCTS ON SALE ROUTES */

//TODO for the addres join users the address_id coclumn in the product_bid table
app.get('/prodcuts-on-sale/purchase-details/:id',function(req, res) {
  pool.query('SELECT  p.name,pb.id AS bid_id, pb.created_at AS bid_date,pb.quantity_purchased,pa.*, u.*,ca.* FROM products p INNER JOIN product_auctions pa ON p.id = pa.product_id  INNER JOIN product_bids pb ON p.id = pb.product_id INNER JOIN users u ON pb.customer_id = u.id INNER JOIN customer_addresses AS ca  ON u.id = ca.customer_id     WHERE pa.id = $1 AND ca.deleted = $2',[req.params.id,"FALSE" ])
  .then(function(results){
    var purchases = results.rows;
    return res.render('business/products-on-sale/purchase-details',{pageTitle:'Resumen de subasta', purchases: purchases});
  })
  .catch(function(error){
    console.log(error);
    return res.json({error: error.message});
  });

});


app.get('/products-on-sale',function(req, res) {
  pool.query('SELECT  p.* FROM products p INNER JOIN product_auctions pa ON p.id = pa.product_id    WHERE p.seller_id = $1 AND pa.visible_in_store = $2',[req.session.user.id, 'true'])
  .then(function(results){
    var products = results.rows;
    return res.render('business/products-on-sale/index',{pageTitle:'Productos en tienda', products: products});
  })
  .catch(function(error){
    console.log(error);
    return res.json({error: error.message});
  });

});


app.get('/products-on-sale/show/:id',function(req, res) {
  pool.query('SELECT  p.name AS name,pa.* FROM products p INNER JOIN product_auctions pa ON p.id = pa.product_id    WHERE p.id = $1 ORDER BY created_at DESC',[req.params.id])
  .then(function(results){
    var products = results.rows;
    return res.render('business/products-on-sale/show',{pageTitle:'Productos en tienda', products: products});
  })
  .catch(function(error){
    console.log(error);
    return res.json({error: error.message});
  });

});

/* END: BUSINESS  PRODUCTS ON SALE ROUTES */

/* START: BUSINESS  BANK ACCOUNTS ROUTES */

app.post('/bank-accounts/update',function(req, res) {
  pool.query('UPDATE  bank_accounts SET bank_name = $1,account_name = $2,account_number = $3,seller_remarks = $4, updated_at = $5 WHERE seller_id = $6',[req.body.bankName,req.body.accountName,req.body.accountNumber,req.body.sellerRemarks,new Date(),req.session.user.id])
  .then(function(results){
    return res.json({message:'Cuanta acutualizada', redirectTo:'/bank-accounts'});
  })
  .catch(function(error){
    console.log(error);
   return res.json({error: error.message});
  });

});

app.get('/bank-accounts/edit/:id',function(req, res) {
  pool.query('SELECT  * FROM bank_accounts  WHERE seller_id = $1',[req.session.user.id])
  .then(function(results){
    var account = results.rows[0];
    return res.render('business/bank-accounts/edit',{pageTitle:'Modificar los datos de su cuenta bancaria',account: account});
  })
  .catch(function(error){
    console.log(error);
   return res.json({error: error.message});
  });

});

app.post('/bank-accounts/create',function(req, res) {
    console.log(req.body);
    pool.query('INSERT INTO bank_accounts (bank_name,account_name,account_number,seller_remarks,seller_id, created_at) VALUES ($1,$2,$3,$4,$5,$6)',[req.body.bankName,req.body.accountName,req.body.accountNumber,req.body.sellerRemarks,req.session.user.id,new Date()])
    .then(function(results){
      var accounts = results.rows;
      return res.json({message:'Cuanta guardada', redirectTo:'/bank-accounts'});
    })
    .catch(function(error){
      console.log(error);
     return res.json({error: error.message});
    });

});


app.get('/bank-accounts',function(req, res) {
  pool.query('SELECT  * FROM bank_accounts  WHERE seller_id = $1',[req.session.user.id])
  .then(function(results){
    var accounts = results.rows;
    return res.render('business/bank-accounts/index',{pageTitle:'Cuenta bancaria',accounts: accounts});
  })
  .catch(function(error){
    console.log(error);
   return res.json({error: error.message});
  });

});

app.get('/bank-accounts/create',function(req, res) {
    return res.render('business/bank-accounts/create',{pageTitle:'Añada su cuenta bancaria'});
});


/* END: BUSINESS  BANK ACCOUNTS ROUTES */


/* START: BUSINESS  CREATE PRODUCT IMAGES  ROUTES */
app.post('/product-images/create',function(req, res) {

    //Create anew filename

var newFileName = randomstring.generate({
   length: 20
});

        upload(req,res,function(err) {
          console.log(req.body);
          console.log(req.files);
            console.log(req.files[0].path);
            if(req.files.length){


  cloudinary.uploader.upload(req.files[0].path, function(image){
   // "eager" parameter accepts a hash (or just a single item). You can pass
   // named transformations or transformation parameters as we do here.

   console.log("** Eager Transformations");
   //console.log(image);
   //if (err){ console.warn(err);}
   //console.log(" * " + image);
   //console.log("* "+image.public_id);
   //console.log("* "+image.eager[0].secure_url);
   waitForAllUploads(image);

},  { public_id : "product_images/" + newFileName, resource_types: 'image'});

function waitForAllUploads( image){
  console.log(image);

    return knex('product_images')
       .insert({product_id:req.body.product_id,
                url_one: JSON.stringify(image), url_one_public_id:image.public_id })
       .then(function(response){

        //delete image from file system
          var filePath = req.files[0].path ;
                fs.unlinkSync(filePath);
        console.log(response);
        return res.json(response);
    })
    .catch(function(err){
         console.log(err);
         var filePath = req.files[0].path ;
                fs.unlinkSync(filePath);
       return  res.json(err);
    });

/*
    //TODO Delete image from DB befor update
    User.findOne({_id:req.user._id}, function(err, user){

        if(!err){
            cloudinary.api.delete_resources([user.avatar_cloudinary_id],
                function(result){
                 //   console.log(result);
                }, { keep_original: false });
        }else{

        }
    });
*/

}
      }

   if(err) {
       return res.end("Error uploading file.");
   }
});


    if(req.body.product_id){

    }


});


app.post('/product-images/update',function(req, res) {


});

app.get('/product-images/:id/update',function(req, res) {


});

app.post('/product-images/:id/delete',function(req, res) {

});

/* END: BUSINESS  CREATE PRODUCT IMAGES  ROUTES */



/* START: BUSINESS  CREATE PRODUCT AUCTIONS  ROUTES */
app.post('/product-auctions/create',function(req, res) {
 console.log(req.body);

    if(req.body.product_id){
       return knex('product_auctions')
       .insert({
          product_id: req.body.product_id,
          price: req.body.price,
          discount_quantity_one: req.body.discount_quantity_one,
          discount_quantity_two: req.body.discount_quantity_two,
          discount_quantity_three: req.body.discount_quantity_three,
          discount_quantity_four: req.body.discount_quantity_four,
          discount_price_one: req.body.discount_price_one,
          discount_price_two: req.body.discount_price_two,
          discount_price_three: req.body.discount_price_three,
          discount_price_four: req.body.discount_price_four,
          limit_per_purchase: req.body.limit_per_purchase,
          total_quantity: req.body.total_quantity,
          start_date: req.body.start_date,
          end_date: req.body.end_date,
          completed: req.body.completed,
          is_local_sale: req.body.is_local_sale,

        })
 .returning('id')
 .then(function(id){
     console.log(id[0]);
   //return   res.json({message: '/product'});
            return res.end('Guardado!');
 })
 .catch(function(err){
      return console.log(err);
 });
    }


});


app.post('/product-auctions/update',function(req, res) {


});

app.get('/product-auctions/update',function(req, res) {


});

app.post('/product-auctions/:id/delete',function(req, res) {


});


/* END: BUSINESS  CREATE PRODUCT AUCTIONS  ROUTES */

/* START: BUSINESS  CREATE PRODUCTS ROUTES */

app.post('/products/create',function(req, res) {
    //console.log(req.body);
    var is_new;
   var titleSlug = slug(req.body.name);
    if(req.body.is_new == 'No'){
        is_new = false;
    }else{
          is_new = true;
    }
    //TODO Add the ID of the newly created product to thge slug
    knex('products')
    .insert({
             seller_id: req.session.user.id,
             name: req.body.name,
            slug: titleSlug,
            category:req.body.category,
            sub_category:  req.body.sub_category,
            type:  req.body.type,
            is_new: is_new,
            ref: req.body.ref,
            sku: req.body.sku,
            manufacturer: req.body.manufacaturer,
            short_description: req.body.short_description,
            long_description: req.body.long_description,
           })
    .returning('id')
    .then(function(id){

        //console.log(id[0]);
        var productId = id[0];
        titleSlug = titleSlug+'-'+productId;
        //console.log(titleSlug);
         knex('products')
        .where({id:productId})
         .update({slug: titleSlug})
         .then(function(data){
             //console.log(data);
         })
         .catch(function(err){
             //console.log(err);
          });
      return   res.json({message: '/products'});
    })
    .catch(function(err){
         console.log(err);
    });


  });

app.get('/products/create',function(req, res) {
        res.render('business/product/create', {});
    });

app.get('/products/show/:id',function(req, res) {
        res.render('business/product/show', {});
    });


app.get('/products',function(req, res) {

    if(req.session.user){
        knex('products')
            .where({seller_id: req.session.user.id})
                .then(function(results){
                    console.log(results);
                res.render('business/product/index', {products: results});
                })
                .catch(function(err){
                    console.log(err);
                });
    }

    });

/* END: BUSINESS  CREATE PRODUCTS  ROUTES */


/* START: BUSINESS  DASHBOARD ROUTES */
app.get('/business-dashboard',function(req, res){
var currentMonthSales =   pool.query("SELECT TO_CHAR(SUM(pa.final_price * pb.quantity_purchased ), '999,999,999.99') AS total_current_month FROM product_bids AS pb  JOIN product_auctions AS pa ON  pb.product_id = pa.product_id  WHERE  pa.product_id IN (SELECT  id FROM products where seller_id= $1) AND pa.completed = $2 AND EXTRACT(MONTH FROM pb.created_at) = EXTRACT(MONTH FROM current_date)",[req.session.user.id, 'TRUE']).then();
var totalSalesCompleted =   pool.query("SELECT COUNT(pa.id) AS total_items_sold FROM products AS p JOIN product_auctions AS pa ON  p.id = pa.product_id WHERE  p.seller_id = $1 AND pa.completed = TRUE ",[req.session.user.id]).then();
var itemsCurrentlyInStore =   pool.query("SELECT COUNT(pa.id) AS total_items_on_sale FROM products AS p JOIN product_auctions AS pa ON  p.id = pa.product_id WHERE  p.seller_id = $1 AND pa.completed = FALSE",[req.session.user.id]).then();

  promise.all([currentMonthSales,totalSalesCompleted,itemsCurrentlyInStore])
 .spread(function(currentMonthSales,totalSalesCompleted,itemsCurrentlyInStore){

     var total_current_month   = (currentMonthSales.rows[0].total_current_month === null ) ? 0 : currentMonthSales.rows[0].total_current_month;
     //var total_sales_completed = (currentMonthSales.rows[0] === null ) ? currentMonthSales.rows[0] : 0;
     //var items_currently_in_store = (currentMonthSales.rows[0] === null ) ? currentMonthSales.rows[0] : 0;
   return res.render('business/dashboard',{
     total_current_month:total_current_month,
     total_sales_completed: totalSalesCompleted.rows[0].total_items_sold,
     items_currently_in_store: itemsCurrentlyInStore.rows[0].total_items_on_sale
   });

  })
  .catch(function(err){
    return console.log(err);
  });


});
-
/* END: BUSINESS  DASHBOARD  ROUTES */

/* START: BUSINESS  LOGIN ROUTES */
app.get('/login-business',function(req, res) {
       return res.render('business/login/index');
    });



 app.post('/login-business',function(req, res) {
            console.log(req.body);
        if(req.body.password && req.body.email){
            var pass = trim(req.body.password);
            console.log(pass);
         var query =    knex.from('users')
               .where({'email': req.body.email})
               .first();
            return query.then(function(results) {
               var  user = {};
                 if(bcrypt.compareSync(pass, results.password)){

                     user.id        = results.id;
                     user.username  = results.username;
                     user.email     =  results.email;
                     user.is_seller = results.is_seller;

                     req.session.user = user;
                    if (results.is_seller === true){
                       return  res.redirect('/business-dashboard');
                    }else{
                       return  res.redirect('/customer-dashboard');
                    }

                  }else{
                     res.json({Error:'Email/contraseña no valido'});
                  }

              })
               .catch(function(err) {
              console.log( err);
                return res.json({error:err.message}) ;
            });


        }else{
             return  res.redirect('/login-business');
        }

        });


/* END: BUSINESS  LOGIN ROUTES */


/* START: BUSINESS  SIGNUP ROUTES */
  app.post('/signup-business',function(req, res) {
        //console.log(req.body);
    if(req.body.password && req.body.name && req.body.email){
       //var hashedPass = passHash.generate(req.body.password);

        //Check if email exists
        var user = new User();
        user.set('username', req.body.name );
        user.set('email', req.body.email );
        user.set('password', bcrypt.hashSync(req.body.password));
        user.set('is_seller', true);
        user.set('provider','password');
        user.set('provider_id', null);
        user.save()
            .then(function(newUser){
            //console.log(newUser);
            //Send conformation request email to user
            return res.redirect('/login-business');
        });

    }else{
        res.json({error: 'Hay campos obligatorios que estan vacios'});
    }

    });


app.get('/signup-business',function(req, res) {
        res.render('business/signup/index', {
            pageTitle: 'Iniciar Sesión'
        });
    });

/* END: BUSINESS SIGNUP  ROUTES */






/* START: ASK A QUESTION  ABOUT A PRODUCT  ROUTES */

app.post('/customer-product-questions/create', function(req, res){
console.log(req.body);

if(req.body.product_id && req.body.question ){
  pool.query('INSERT INTO product_questions (product_id,asked_by,question,created_at) VALUES ($1,$2,$3,$4)',[req.body.product_id, req.session.user.id, req.body.question, new Date()])
  .then(function(results){
    return res.json({
        message: 'Pregunta enviada',
    });
  })
  .catch(function(err){
    console.log(err);
    return res.json({error:err.message});

  });
}else{
  return res.json({error: 'Los campos obligatorios no deberian estar vacios'});
}


});

/* END: ASK A QUESTION  ABOUT A PRODUCT  ROUTES */



/* START: CUSTOMER PAYMENT CARD ROUTES */
app.get('/customer-payment', function(req, res){

  pool.query('SELECT * FROM customer_payment_info WHERE customer_id = $1',[req.session.user.id])
  .then(function(results){

    return res.render('customer/payment/index', {
        pageTitle: 'Tarjeta de pago',
        cards: results.rows
    });
  })
  .catch(function(err){
    console.log(err);
    return res.json({error:err.message});

  });

});



app.get('/customer-payment/edit/:id', function(req, res){
  console.log(req.params.id);
  pool.query('SELECT * FROM customer_payment_info WHERE customer_id = $1',[req.session.user.id])
  .then(function(results){
     console.log(results.rows[0]);
     var card = results.rows[0];
     return res.render('customer/payment/edit', {card:card});
  })
  .catch(function(err){
    //console.log(err);
    return res.json({error:err.message});
  });

});

app.post('/customer-payment/update/', function(req, res){
  //console.log(req.body);

  stripe.customers.update(req.body.cus_id, {
    source: {object:'card',number:req.body.number, exp_month:req.body.exp_month,exp_year:req.body.exp_year,cvc: req.body.cvc   },
  }, function(err, customer) {
    // asynchronously called
    //(customer_id, stripe_customer_id, ,card_brand,card_last4,card_exp_month,card_exp_year,card_cvc_check, card_country_code,client_ip, created_at
var cutomerInfo = customer.sources.data[0];
var ip = req.connection.remoteAddress ||  req.headers['x-forwarded-for'];
pool.query('UPDATE customer_payment_info SET  stripe_card_id = $1, card_brand = $2,card_last4 = $3,card_exp_month = $4, card_exp_year = $5,card_cvc_check = $6, card_country_code = $7, client_ip = $8, updated_at = $9  WHERE customer_id = $10',[cutomerInfo.id, cutomerInfo.brand,cutomerInfo.last4,cutomerInfo.exp_month,cutomerInfo.exp_year,cutomerInfo.cvc_check,cutomerInfo.country,ip,new Date(),req.session.user.id])
.then(function(results){
  //console.log(customer.sources.data);
  return res.json({message: 'Tarjeta actaualizada', redirectTo:'/customer-payment'});
})
.catch(function(err){
  //console.log(err);
  return res.json({error:err.message});
});

  });



});

app.get('/customer-payment/create', function(req, res){
  return res.render('customer/payment/create', {
      pageTitle: 'Nueva tarjeta de pago'
  });
});

app.post('/customer-payment/create', function(req, res){
   //console.log(req.body);
  if(!req.body){
   return res.json({
        error: 'Error en la recepción de los datos, intentelo de nuevo'
    });
  }else{

//GET IP
var ip = req.connection.remoteAddress ||  req.headers['x-forwarded-for'];
 //console.log(ip);
 //console.log('USer ID: ',req.session.user.id );

    stripe.customers.create({
      description: 'Customer for flibuy with user_id: ' + req.session.user.id ,
      source: {object:'card',number:req.body.number, exp_month:req.body.exp_month,exp_year:req.body.exp_year,cvc: req.body.cvc   }, // obtained with Stripe.js
      email: req.session.user.email,
      metadata:{flibuy_user_id:req.session.user.id }
    }, function(err, customer) {
      // asynchronously called
      if(customer){
        var cutomerInfo = customer.sources.data[0];
        pool.query('INSERT INTO customer_payment_info (customer_id, stripe_customer_id, stripe_card_id,card_brand,card_last4,card_exp_month,card_exp_year,card_cvc_check, card_country_code,client_ip, created_at) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)',[req.session.user.id,customer.id, cutomerInfo.id, cutomerInfo.brand,cutomerInfo.last4,cutomerInfo.exp_month,cutomerInfo.exp_year,cutomerInfo.cvc_check,cutomerInfo.country,ip,new Date()])
        .then(function(results){
          console.log(results);
            return res.json({message:'Tarjeta guardada', redirectTo:'/customer-payment'});
        })
        .catch(function(err){
            console.log(err);
              return res.json({error:err.message});
        });
      }else{
          console.log(err);
          return res.json({error:err.message});
      }


    });

  }

});

/* END: CUSTOMER PAYMENT CARD ROUTES */





/* START: CUSTOMER ADDRESS ROUTE */

app.get('/customer-address', function(req, res) {

  pool.query('SELECT * FROM customer_addresses WHERE customer_id = $1 AND deleted = $2',[req.session.user.id, false])
  .then(function(address){
       //console.log(address.rows[0]);
      return res.render('customer/address/index', {
                pageTitle: 'Lista de direcciones de envio',
                addresses: address.rows
                });
  })
  .catch(function(error){
       //console.log(error);
       return res.json({error:error.message});
    });

});

app.get('/customer-address/create', function(req, res) {
    return res.render('customer/address/create', {
                  pageTitle: 'Añada una dirección',
                });
});

app.post('/customer-address/create', function(req, res) {
     //console.log(req.body);
    if(!req.session ){
     return res.json({'error': 'Debe iniciar sesión para continuar'});
    }
     pool.query('INSERT INTO customer_addresses (customer_id,line_one,city,post_code,state,country,other_details,created_at,recipient_name) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9) ',[req.session.user.id,req.body.street,req.body.city,req.body.post_code,req.body.province,req.body.country,req.body.other_details, new Date(), req.body.name])
    .then(function(results){
        // console.log(results);
          return res.json({'message': 'Dirección guardado', redirectTo: '/customer-address'});
     })
     .catch(function(err){
        // console.log(err);
          return res.json({'error': err.message});
     });

});

app.get('/customer-address/edit/:id', function(req, res){
  if(!req.params.id){
    return;
  }

  pool.query('SELECT * FROM customer_addresses WHERE id = $1 AND customer_id = $2', [req.params.id, req.session.user.id])
  .then(function(results){
   //console.log(results.rows[0]);
   return res.render('customer/address/edit', {
                 pageTitle: 'Modificar dirección',
                 address: results.rows[0]
               });
  })
  .catch(function(err){
      //console.log(err);
      return res.json({'error': err.message});
  });

});

app.post('/customer-address/update', function(req, res){
  console.log(req.body);
//  console.log(req.session.user);
// new Date,
  pool.query('UPDATE  customer_addresses SET line_one = $1, city = $2,  post_code = $3, state = $4, country = $5, other_details = $6, recipient_name = $7, updated_at = $8 WHERE  id = $9', [req.body.street, req.body.city, req.body.post_code, req.body.province, req.body.country, req.body.other_details, req.body.name, new Date(), req.body.address_id])
  .then(function(results){
   //console.log(results);
   return res.json({'message': 'Dirección actualizado', redirectTo: '/customer-address'});
  })
  .catch(function(err){
      return res.json({'error': err.message});
  });

});


app.delete('/customer-address/delete', function(req, res){
//  console.log(req.body.address_id);
//TODO check if there is no sale associated with the address the delete it else set deleted as true
  pool.query('UPDATE  customer_addresses SET deleted = $1 ,  updated_at = $2 WHERE  customer_id = $3 AND id = $4', [ true, new Date(), req.session.user.id, req.body.address_id])
  .then(function(results){
   //console.log(results);
   return res.json({'message': 'Dirección actualizado'});
  })
  .catch(function(err){
    //  console.log(err);
      return res.json({'error': err.message});
  });
   return res.json({message:'Direccion eliminado'});
});

/* END: CUSTOMER ADDRESS  ROUTES */





/* START:  CUSTOMER PRODUCTS IN PURCHASE PROCESS ROUTE */

//
app.get('/products-purchase-in-progress', function(req, res){
    var SQL = 'SELECT products.*, product_bids.id AS bid_id,product_bids.created_at AS purchase_date, product_bids.quantity_purchased AS purchased, product_auctions.*  FROM products INNER JOIN product_bids ON product_bids.product_id = products.id  INNER JOIN product_auctions ON product_auctions.product_id = products.id  WHERE product_bids.customer_id = $1 AND product_auctions.completed = $2 ';

    pool.query(SQL,[req.session.user.id, "FALSE"])
        .then(function(results){
        //console.log(results.rows);
         res.render('customer/products/in-purchase',{products: results.rows});
    })
    .catch(function(error){
           //console.log(error);
          return  res.json({error:error.message});
     });

});
/* END: CUSTOMER PRODUCTS IN PURCHASE PROCESS  ROUTE */


/* START:  CUSTOMER DASHBOARD ROUTE */

app.get('/customer-dashboard',function(req, res){

    res.render('customer/dashboard');
});
/* END:  CUSTOMER DASHBOARD ROUTE */



/* START:  MARK PRODUCT AUCTION AS COMPLETED  ROUTE */
app.post('/sale-completed', function(req, res){
    console.log(req.body);


    pool.query('UPDATE product_auctions SET completed = $1, visible_in_store = $2 WHERE id = $3',['TRUE','FALSE' ,req.body.product_auction_id])
    .then(function(data){
        //TODO
        //Check if thare are more aunctions and set the next to visible in store = TRUE
        console.log(data);
        return res.json({message:'Venta con #REF ' + req.body.product_auction_id + ' ha finalizada'});
    })
    .catch(function(error){
          //return console.log(error);
          return  res.json({error:error.message});
     });

});

/* END:  MARK PRODUCT AUCTION AS COMPLETED  ROUTE */

/* START:  BIDS ROUTES */

  app.post('/bid', function(req, res){
    console.log(req.body);



    //Check for user session
     if(!req.session.user){
        return res.json({error:'Debe iniciar sesión para continuar!'});
     }
    //Validations of input data

    // Check if the user is not already buying the product

    //CHeck for payment card

    //Notify users purchase is OK
    res.json({message: 'Compra realizada'});

    pool.query('INSERT INTO product_bids (customer_id, product_id,product_auctions_id,quantity_purchased,created_at) VALUES ($1,$2,$3,$4,$5) ',[req.session.user.id,req.body.product_id,req.body.product_auction_id,req.body.quantity_purchased, new Date()])
    .then(function(results){
       //return console.log(results);
        //GEt totoal sold
         pool.query('SELECT SUM(quantity_purchased) AS total_sold FROM product_bids WHERE product_auctions_id = $1',[req.body.product_auction_id])
         .then(function(results){
           //return console.log(results.rows[0].total_sold);
             var totalSold = results.rows[0].total_sold;


                 pool.query('SELECT *  FROM product_auctions WHERE id = $1 AND visible_in_store = $2',[req.body.product_id, 'TRUE'])
                 .then(function(results){
                   var product = results.rows[0];

                     //Chek if the purchase is not more than the
                       if(totalSold > product.total_quantity){
                         return res.json({message: 'La cantidad que quires comprar es superior a las unidades restantes'});

                         //TODO rollback the previous insert here
                        }


                      //DISCOUNT 1
                     if(totalSold >= product.discount_quantity_one && totalSold < product.discount_quantity_two){
                         //TODO use the final price cocumn insted of the price column
                         //SET the price to
                         console.log('Price one updating..');
                         pool.query('UPDATE   product_auctions  SET final_price = $2 WHERE id = $1',[req.body.product_id, product.discount_price_one])
                         .then(function(data){
                            // return console.log(data);
                            //EMIT product Chages to conected clients
                             return io.emit('product_bid', {
                                 totalSold : totalSold,
                                 currentPrice : product.discount_price_one,
                                 auctionId: req.body.product_auction_id
                             });
                         })
                         .catch(function(error){
                           return console.log(error);
                          });
                     }

                     //DISCOUNT 2
                       if(totalSold >= product.discount_quantity_two && totalSold < product.discount_quantity_three){
                         //TODO use the final price cocumn insted of the price column
                         //SET the price to
                         console.log('Price two updating..');
                         pool.query('UPDATE   product_auctions  SET final_price = $2 WHERE id = $1',[req.body.product_id, product.discount_price_two])
                         .then(function(data){
                             //return console.log(data);
                            //EMIT product Chages to conected clients
                      return io.emit('product_bid', {
                          totalSold : totalSold,
                          currentPrice : product.discount_price_two,
                          auctionId: req.body.product_auction_id
                      });
                         })
                         .catch(function(error){
                           return console.log(error);
                          });
                     }

                     //DISCOUNT 3
                       if(totalSold >= product.discount_quantity_three && totalSold < product.discount_quantity_four){
                         //TODO use the final price cocumn insted of the price column
                         //SET the price to
                         console.log('Price three updating..');
                         pool.query('UPDATE   product_auctions  SET final_price = $2 WHERE id = $1',[req.body.product_id, product.discount_price_three])
                         .then(function(data){
                            // return console.log(data);
                             //EMIT product Chages to conected clients
                     return io.emit('product_bid', {
                         totalSold : totalSold,
                         currentPrice : product.discount_price_three,
                          auctionId: req.body.product_auction_id
                     });
                         })
                         .catch(function(error){
                           return console.log(error);
                          });
                     }

                     //DISCOUNT 4
                       if( totalSold >= product.discount_quantity_four){
                         //TODO use the final price cocumn insted of the price column
                         //SET the price to
                         console.log('Price four updating..');
                         pool.query('UPDATE   product_auctions  SET final_price = $2 WHERE id = $1',[req.body.product_id, product.discount_price_four])
                         .then(function(data){
                             //return console.log(data);
                             //EMIT product Chages to conected clients
                           return io.emit('product_bid', {
                               totalSold : totalSold,
                               currentPrice : product.discount_price_four,
                                auctionId: req.body.product_auction_id
                           });
                         })
                         .catch(function(error){
                           return console.log(error);
                          });
                     }



                 })
                 .catch(function(error){
                   return console.log(error);
                  });
         })
         .catch(function(error){
           return console.log(error);
          });
    })
    .catch(function(error){
       return console.log(error);
    });
});

app.get('/cancel-bid/:bid_id', function(req, res){
     //console.log(req.params.bid_id);
     //console.log(req.body);
      res.render('customer/products/cancel-purchase');
    //res.json({'hi': req.params.bid_id});
});

/* END:  BIDS ROUTES */


/* START:  CUSTOMER LOGOUT ROUTE */

app.get('/logout',function(req, res) {
        if(req.session){
            req.session.destroy();
            req.session = null;
            //delete req.session;
           return res.redirect('/shop');
        }else{
             return res.redirect('/shop');
        }

    });

/* END:  CUSTOMER LOGOUT ROUTE */



/* START:  CUSTOMER LOGIN ROUTES */
  app.post('/login-customer',function(req, res) {
        console.log(req.body);
    if(req.body.password && req.body.email){
     var query =    knex.from('users')
           .where({'email': trim(req.body.email)})
           .first();
        return query.then(function(model) {
             //console.log(model);


             if(bcrypt.compareSync(req.body.password,  model.password)){

                    delete model.password;
                    delete model.created_at;
                    delete model.updated_at;

                  req.session.user = model;
                  console.log(req.session.user );
                 return  res.json({redirectTo:'/customer-dashboard'});
              }else{
                 return res.json({error:' Su Email/contraseña no es válido'});
              }

          })
           .catch(function(err) {
           console.log( err);
            return res.json({error: err.message}) ;
        });


    }else{
        res.json({error: 'Introduzca su email/contraseña'});
    }

    });

 app.get('/login-customer',function(req, res) {
    //return res.json(req.body);
       return  res.render('customer/login/index', {});
    });




/* END:  CUSTOMER LOGIN  ROUTES */




//start server
server.listen(port, function(){
    console.log('Listening on port ' + port);
    //Auto refresh browser on reload
    if (process.send) {
        process.send('online');
    }

});



//Development error handling
// will print stacktrace
if (enviroment === 'development' || enviroment === 'undefined') {
    app.use(function(err, req, res, next) {

        console.log(err);
        res.status( err.code || 500 )
            .json({
                status: 'error',
                message: err
            });
          return knex.destroy();
    });
}

//Production error handling
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500)
        .json({
            status: 'error',
            message: err.message
        });
});
