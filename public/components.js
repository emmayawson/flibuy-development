/* START DISPLAY TOAST NOTIFICATIONS  */

 function ErrorToast (message){
    return $.toast({  heading: 'Oops!',
                        text : message, 
                        showHideTransition : 'slide',  // It can be plain, fade or slide
                        position : 'top-right'  ,
                        icon: 'error'
                    });
};

 function SuccessToast (message){
    return $.toast({  heading: 'OK!',
                        text : message, 
                        showHideTransition : 'slide',  // It can be plain, fade or slide
                        position : 'top-right'  ,
                        icon: 'success'
                    });
};

/*  END DISPLAY TOAST NOTIFICATIONS  */