$(document).ready(function(){
    console.log('JQ loaded...');

    //Instanciate Tootipster to display tooltips
    //Not beign used at the moment because it craete conflicts
    /*
    $('.tooltip').tooltipster({
            theme: 'tooltipster-shadow',
     });
    */




    //UPDATE CUSTOMER ADDRESS
    $('#edit_business_address_btn')
        .on('click', function(evt){
         evt.preventDefault();
         var address_id = $('#address_id').val();
         var address_type = $('#address_type').val();
         var name = $('#name').val();
         var street = $('#street').val();
         var city = $('#city').val();
         var post_code = $('#post_code').val();
         var province = $('#province').val();
         var country = $('#country').val();
         var other_details = $('#other_details').val();

         if(!address_id){
             return ErrorToast('No hemos podido reconocer esta direccion, recarga la pagina y prueba de nuevo !');
         }

         if(!address_type){
             return ErrorToast('Introduzca el tipo de dirección !');
         }
        if(!name){
            return ErrorToast('Introduzca el nombre de la tienda !');
        }
        if(!street){
            return ErrorToast('Introduzca la calle y número !');
        }
         if(!city){
            return ErrorToast('Introduzca la ciudad!');
        }
         if(!post_code){
            return ErrorToast('Introduzca el código postal!');
        }
         if(!province){
            return ErrorToast('Introduzca la provincia!');
        }
         if(!country){
            return ErrorToast('Introduzca el país!');
        }

        var data  = {
            address_type:address_type,
            address_id:address_id,
            name:name,
            street:street,
            city: city,
           post_code:post_code,
           province:province,
            country:country,
            other_details:other_details
        };
        $.post("/business-address/update", data,function() {
           //Display loading gif
        })
        .done(function(response) {
            if(response.message){
                window.location.href = response.redirectTo;
                return SuccessToast(response.message);
            }
            if(response.error){
                return ErrorToast(response.error);
            }

        })
        .fail(function(error) {
           return ErrorToast(error.message);
        });

    });


//CREATE RPDUCT PICKIP LOCATION ADDRESS

$('#create_business_address_btn')
    .on('click', function(evt){
     evt.preventDefault();
     var address_type = $('#address_type').val();
     var name = $('#name').val();
     var street = $('#street').val();
     var city = $('#city').val();
     var post_code = $('#post_code').val();
     var province = $('#province').val();
     var country = $('#country').val();
     var other_details = $('#other_details').val();

     if(!address_type){
         return ErrorToast('Introduzca el tipo de dirección !');
     }
    if(!name){
        return ErrorToast('Introduzca el nombre de la tienda !');
    }
    if(!street){
        return ErrorToast('Introduzca la calle y número !');
    }
     if(!city){
        return ErrorToast('Introduzca la ciudad!');
    }
     if(!post_code){
        return ErrorToast('Introduzca el código postal!');
    }
     if(!province){
        return ErrorToast('Introduzca la provincia!');
    }
     if(!country){
        return ErrorToast('Introduzca el país!');
    }

    var data  = {
      address_type:address_type,
        name:name,
        street:street,
        city: city,
       post_code:post_code,
       province:province,
        country:country,
        other_details:other_details
    };
    $.post("/business-address/create", data,function() {
       //Display loading gif
    })
    .done(function(response) {
        if(response.message){
            window.location.href = response.redirectTo;
            return SuccessToast(response.message);
        }
        if(response.error){
            return ErrorToast(response.error);
        }

    })
    .fail(function(error) {
       return ErrorToast(error.message);
    });

});



//UPDATE SELLER BANK ACCOUNT
$('#edit_bank_account_btn')
.on('click', function(evt){
   evt.preventDefault();
   var accountName   = $('#account_name').val();
   var bankName       = $('#bank_name').val();
   var accountNumber = $('#account_number').val();
   var sellerRemarks = $('#seller_remarks').val();

   //console.log(product_id,question );
   if(!accountName){
       return ErrorToast('Introduzca el nombre del titular de la cuenta');
   }
   if(!accountNumber){
       return ErrorToast('Introduzca el número  de la cuenta');
   }
   if(!bankName){
       return ErrorToast('Introduzca el nombre del banco');
   }



   $.ajax({
     type:'POST',
     url:'/bank-accounts/update',
     dataType:'JSON',
     data:{accountName: accountName,accountNumber: accountNumber, sellerRemarks:sellerRemarks, bankName:bankName }
   })
   .done(function(response) {
       //console.log(response);
       if(response.message){
           window.location.href = response.redirectTo;
           return SuccessToast(response.message);
       }
       if(response.error){
           return ErrorToast(response.error);
       }

   })
   .error(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   })
   .fail(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   });

});

//CREATE SELLER BANK ACCOUNT
$('#create_bank_account_btn')
.on('click', function(evt){
   evt.preventDefault();
   var accountName   = $('#account_name').val();
   var bankName       = $('#bank_name').val();
   var accountNumber = $('#account_number').val();
   var sellerRemarks = $('#seller_remarks').val();

   //console.log(product_id,question );
   if(!accountName){
       return ErrorToast('Introduzca el nombre del titular de la cuenta');
   }
   if(!accountNumber){
       return ErrorToast('Introduzca el número  de la cuenta');
   }
   if(!bankName){
       return ErrorToast('Introduzca el nombre del banco');
   }



   $.ajax({
     type:'POST',
     url:'/bank-accounts/create',
     dataType:'JSON',
     data:{accountName: accountName,accountNumber: accountNumber, sellerRemarks:sellerRemarks, bankName:bankName }
   })
   .done(function(response) {
       //console.log(response);
       if(response.message){
           window.location.href = response.redirectTo;
           return SuccessToast(response.message);
       }
       if(response.error){
           return ErrorToast(response.error);
       }

   })
   .error(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   })
   .fail(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   });

});




//ASK A QUESTION ABOUT A PRODUCT
$('#product_question_btn')
.on('click', function(evt){
   evt.preventDefault();
   var product_id = $('#question_product_id').val();
   var question = $('#question').val();

   //console.log(product_id,question );
   if(!product_id){
       return ErrorToast('No hemos podido identificar el producto, recarga la pagina y intentelo de nuevo');
   }
   if(!question){
       return ErrorToast('Introduzca una pregunta');
   }


   $.ajax({
     type:'POST',
     url:'/customer-product-questions/create',
     dataType:'JSON',
     data:{product_id: product_id,question: question }
   })
   .done(function(response) {
       //console.log(response);
       if(response.message){
           //window.location.href = response.redirectTo;
           return SuccessToast(response.message);
       }
       if(response.error){
           return ErrorToast(response.error);
       }

   })
   .error(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   })
   .fail(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   });

});


//STRIPE UPDATE CARD FORM
$('#edit_customer_payment_btn').on('click', function(evt){
   evt.preventDefault();
  $('#edit_customer_payment_btn').prop('disabled', true);
   var cus_id = $('#customer_id').val();
   var number = $('#number').val();
   var exp_month = $('#exp_month').val();
   var exp_year = $('#exp_year').val();
   var cvc = $('#cvc').val();

   if(!cus_id){
       return ErrorToast('No hemos podido identificar esta tarjeta recarga la pagina y prueba de nuevo');
   }
   if(!number){
       return ErrorToast('Introduzca su número de tarjeta');
   }

   if(!exp_month){
       return ErrorToast('Introduzca el mes de caducidad de su tarjeta');
   }
   if(!exp_year){
       return ErrorToast('Introduzca el año de caducidad de su tarjeta');
   }
   if(!cvc){
       return ErrorToast('Introduzca el código CVC de su tarjeta');
   }

   //console.log(number,exp_month,exp_year,cvc);

   $.ajax({
     type:'POST',
     url:'/customer-payment/update/',
     dataType:'JSON',
     data:{cus_id:cus_id,number: number,exp_month:exp_month,exp_year:exp_year,cvc:cvc }
   })
   .done(function(response) {
       console.log(response);
       if(response.message){
           window.location.href = response.redirectTo;
           return SuccessToast(response.message);
       }
       if(response.error){
           return ErrorToast(response.error);
       }

   })
   .error(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   })
   .fail(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   });

});

//STRIPE SUBMIT CARD FORM

$('#create_customer_payment_btn').on('click', function(evt){
   evt.preventDefault();
  $('#create_customer_payment_btn').prop('disabled', true);
   var number = $('#number').val();
   var exp_month = $('#exp_month').val();
   var exp_year = $('#exp_year').val();
   var cvc = $('#cvc').val();

   if(!number){
       return ErrorToast('Introduzca su número de tarjeta');
   }
   if(!exp_month){
       return ErrorToast('Introduzca el mes de caducidad de su tarjeta');
   }
   if(!exp_year){
       return ErrorToast('Introduzca el año de caducidad de su tarjeta');
   }
   if(!cvc){
       return ErrorToast('Introduzca el código CVC de su tarjeta');
   }

   //console.log(number,exp_month,exp_year,cvc);

   $.ajax({
     type:'POST',
     url:'/customer-payment/create',
     dataType:'JSON',
     data:{number: number,exp_month: exp_month,exp_year: exp_year , cvc:cvc }
   })
   .done(function(response) {
       console.log(response);
       if(response.message){
           window.location.href = response.redirectTo;
           return SuccessToast(response.message);
       }
       if(response.error){
           return ErrorToast(response.error);
       }

   })
   .error(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   })
   .fail(function(error) {
       console.log(error);
      return ErrorToast(error.message);
   });

});

    //DELETE CUSTOMER ADDRESS
    $('.delete_customer_address_btn')
     .on('click', function(evt){
          evt.preventDefault();
          var addressId = $(this).attr('data-address-id');
          if(addressId){
            $.ajax({
              type:'DELETE',
              url:'/customer-address/delete/',
              dataType:'JSON',
              data:{address_id: addressId}
            })
            .done(function(response) {
                if(response.message){
                    location.reload();
                    return SuccessToast(response.message);
                }
                if(response.error){
                    return ErrorToast(response.error);
                }

            })
            .fail(function(error) {
               return ErrorToast(error.message);
            });
          }else{
            return ErrorToast('Recarga la página y prueba de nuevo');
          }
     });

    //CREATE CUSTOMER ADDRESS
    $('#create_customer_address_btn')
        .on('click', function(evt){
         evt.preventDefault();
         var name = $('#name').val();
         var street = $('#street').val();
         var city = $('#city').val();
         var post_code = $('#post_code').val();
         var province = $('#province').val();
         var country = $('#country').val();
         var other_details = $('#other_details').val();

        if(!name){
            return ErrorToast('Introduzca el nombre del receptor!');
        }
        if(!street){
            return ErrorToast('Introduzca la calle y número !');
        }
         if(!city){
            return ErrorToast('Introduzca la ciudad!');
        }
         if(!post_code){
            return ErrorToast('Introduzca el código postal!');
        }
         if(!province){
            return ErrorToast('Introduzca la provincia!');
        }
         if(!country){
            return ErrorToast('Introduzca el país!');
        }

        var data  = {
            name:name,
            street:street,
            city: city,
           post_code:post_code,
           province:province,
            country:country,
            other_details:other_details
        };
        $.post("/customer-address/create", data,function() {
           //Display loading gif
        })
        .done(function(response) {
            if(response.message){
                window.location.href = response.redirectTo;
                return SuccessToast(response.message);
            }
            if(response.error){
                return ErrorToast(response.error);
            }

        })
        .fail(function(error) {
           return ErrorToast(error.message);
        });

    });


    //UPDATE CUSTOMER ADDRESS
    $('#edit_customer_address_btn')
        .on('click', function(evt){
         evt.preventDefault();
         var address_id = $('#address_id').val();
         var name = $('#name').val();
         var street = $('#street').val();
         var city = $('#city').val();
         var post_code = $('#post_code').val();
         var province = $('#province').val();
         var country = $('#country').val();
         var other_details = $('#other_details').val();

        if(!name){
            return ErrorToast('Introduzca el nombre del receptor!');
        }
        if(!street){
            return ErrorToast('Introduzca la calle y número !');
        }
         if(!city){
            return ErrorToast('Introduzca la ciudad!');
        }
         if(!post_code){
            return ErrorToast('Introduzca el código postal!');
        }
         if(!province){
            return ErrorToast('Introduzca la provincia!');
        }
         if(!country){
            return ErrorToast('Introduzca el país!');
        }

        var data  = {
            name:name,
            street:street,
            city: city,
           post_code:post_code,
           province:province,
            country:country,
            other_details:other_details,
            address_id:address_id
        };
        $.post("/customer-address/update", data,function() {
           //Display loading gif
        })
        .done(function(response) {
            if(response.message){
                  window.location.href = response.redirectTo;
                return SuccessToast(response.message);
            }
            if(response.error){
                return ErrorToast(response.error);
            }

        })
        .fail(function(error) {
           return ErrorToast(error.message);
        });

    });


    //COUNDOWN TIMER

    $('[data-countdown]').each(function() {
       var $this = $(this),
           finalDate = $(this).data('countdown');
       $this.countdown(finalDate, function(event) {
         $this.html(event.strftime('%D dias %H:%M:%S'));
       })
       .on('finish.countdown', function(event){
           //console.log($this.attr('data-product-auction-id'));
           //console.log(event);
           var auctionId = $this.attr('data-product-auction-id');

          var data = {product_auction_id: auctionId};
         $.post("/sale-completed", data,function() {
           //Display loading gif
        })
        .done(function(response) {
            if(response.message){
                //return  console.log(response.message);
                return SuccessToast(response.message);
            }
            if(response.error){
                return ErrorToast(response.error);
            }

        })
        .fail(function(error) {
           return ErrorToast(error.message);
        });


       });
     });


     //REMOVED A PURCHASED PRODUCT
        $('#cancel_bid').on('click', function(evt){
         evt.preventDefault();
         var productId = $(this).attr('data-product-id');
        console.log(productId);


    });


    //BUY A PRODUCT (ONE)
    $('#bid').on('click', function(evt){
        evt.preventDefault();
        var productId = $(this).attr('data-product-id');
        var productAuctionId = $(this).attr('data-product-auction-id');
        var quantityPurchased = $('#quantity_purchased').val();

        //console.log(productId);
        //console.log(quantityPurchased);
        //console.log(productAuctionId);
        if(!quantityPurchased){
            return ErrorToast('Indique  la cantidad que quires comprar!');
        }
        if(!productId){
            return ErrorToast('Recarga la pagina y vueva a intentarlo!');
        }

        if(!productAuctionId){
            return ErrorToast('Recarga la pagina y vueva a intentarlo!');
        }
        var data = {product_id: productId,quantity_purchased: quantityPurchased, product_auction_id: productAuctionId };
         $.post("/bid", data,function() {
           //Display loading gif
        })
        .done(function(response) {
            if(response.message){
                //return  console.log(response.message);
                return SuccessToast(response.message);
            }
            if(response.error){
                return ErrorToast(response.error);
            }

        })
        .fail(function(error) {
           return ErrorToast(error.message);
        });



    });





    //POST login Customer/buyer
    $('#login_customer_btn').on('click', function(evt){
        evt.preventDefault();
         var email = $('#email').val();
         var password = $('#password').val();
            if(!email){
                return ErrorToast('Introduzca su email');
            }

            if(!password){
                return ErrorToast('Introduzca su contraseña');
            }
            console.log(email, password);

         var data = {
                email: email,
                password: password
            };

        $.post("/login-customer", data,function(data) {
           //Display loading gif
            console.log(data);
        })
        .done(function(response) {
            if(response.redirectTo){
                  window.location.href = response.redirectTo;
                  return;
                //return SuccessToast(response.message);
            }
            if(response.error){
                return ErrorToast(response.error);
            }

        })
        .fail(function(error) {
           return ErrorToast(error.message);
        });

    });

    //POST create new porduct
    $('#create_product_btn').on('click', function(evt){
       evt.preventDefault();
        var dataString = $('#create_product').serialize();
       //alert(dataString);

        $.post("/products/create", dataString,function(data) {

           //Display loading gif
        })
        .done(function(response) {
            window.location.href = response.message;
        })
        .fail(function(error) {
            alert(error);
        });
    });


    //POST create customer user account
        $('#create_customer_btn').on('click', function(evt){
          evt.preventDefault();
          var username = $('#name').val();
          var email = $('#email').val();
          var password = $('#password').val();
          var password_confirm = $('#password_confirm').val();
          //alert( password_confirm);

            if(!username){
                return ErrorToast('Indique su nombre');
            }

            if(!email){
                return ErrorToast('Indique su email');
            }

            if(!password){
                return ErrorToast('Indique su contraseña');
            }
            if(password !== password_confirm){
                return ErrorToast('Sus contraseñas no coinciden, deberían ser iguales!');
            }

            var data = {
                name: username,
                email: email,
                password: password
            };



        $.post("/signup-customer", data,function(data) {

           //Display loading gif
            console.log(data);
        })
        .done(function(response) {
            if(response.message){

                 window.location.href = response.redirectTo;
                return SuccessToast(response.message);
            }
            if(response.error){
                return ErrorToast(response.error);
            }


        })
        .fail(function(error) {
           return ErrorToast(error.message);
        });

    });



});
