//var enviroment = process.env.APP_ENV || 'undefined'; //Set enviroment
var redisUrl = "pub-redis-14552.us-east-1-3.6.ec2.redislabs.com:14552";
var conf = require('./modules/config-loader')(process.env.APP_ENV); //APP_ENV is a custom env var created in heroku
//console.log(conf)
//DB
var knex = require('./database');

//Required modules 


var express = require('express'),
    app = express(),
    server = require('http').Server(app),
    io = require('socket.io')(server),
    fs = require('fs'),
    nunjucks = require('nunjucks'),
    marko = require('marko'),
    bcrypt = require('bcrypt-nodejs'),
    trim = require('trim'),
    multer = require('multer'),
    moment = require('moment'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'), //for processing post body
    morgan = require('morgan'), //Loging req info in dev
    async = require('async'),
    stripe = require("stripe")(conf.STRIPE_SECRET_KEY),
    path = require('path'),
    redis = require("redis"),
    bcrypt = require('bcrypt-nodejs'),
    session = require('express-session'), //Sessions 
    KnexSessionStore = require('connect-session-knex')(session),
    cloudinary  = require('cloudinary');

    cloudinary.config({
    cloud_name: conf.CLOUDINARY_CLOUD_NAME,
    api_key: conf.CLOUDINARY_API_KEY, 
    api_secret: conf.CLOUDINARY_API_SECRET
});
    //RedisStore  = require('connect-redis')(session), //redis store
    //rtg         = require("url").parse(redisUrl),
    //client      = redis.createClient('14552', rtg.hostname),
    //redisAuth   =  rtg.auth.split(":"),
    //client.auth(redisAuth[1]),


//NPM module initializations
var CONCURRENCY = process.env.WEB_CONCURRENCY || 1; //Heroku number of procesors detection
var upload = multer({ dest: 'uploads/' }); //Multer default file upload folder

    module.exports = function (app, enviroment) {


        //App configurations
        if (enviroment === 'undefined' || enviroment === 'development') {
            app.use(morgan('dev'));
        }

        app.use(cookieParser());
        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(bodyParser.json());
        app.use(express.static(path.join(__dirname, 'public'))); //serve static files
        app.use(express.static(path.join(__dirname, 'fonts'))); //serve static files
        app.use(express.static(path.join(__dirname, '/bower_components'))); //serve static files

        //Configur express session using a redis data store
        var MemoryStore = session.MemoryStore;
        var store = new KnexSessionStore({
            knex: knex,
            tablename: 'sessions' // optional. Defaults to 'sessions'
        });
        app.use(session({
            secret: 'secret',
            saveUninitialized: false,
            resave: false,
            store: store
                /* store: new RedisStore({host: rtg.prtotocol, port: rtg.port, client: client})*/

        }));


        //Set view engine 
        app.set('views', 'views');
        app.set('view engine', 'html');

        //Configue nunjucks template engine
        nunjucks.configure('views', {
          autoescape: true,
          express   : app
        });


  //Socket.io conecction
    io.on('connection', function(socket){
       console.log('A Client has connected to the web socket'); 
        socket.emit("message",["Hi", "There"]);
    });

        //Global settings
        //app.locals.basedir          = path.join(__dirname, 'views');
        app.locals.moment = moment;
        app.locals.cloudinary = cloudinary;


        return app;
    };