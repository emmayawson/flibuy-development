// Update with your config settings.

module.exports = {
   
    /*
    development: {
    client: 'pg',
    connection:{
        host: 'horton.elephantsql.com',
        user:'wcceafum',
        database:'wcceafum',
        password:'PJ_mhM_m45IjEL-8-ewZTlCqIxFvwkCc'
            },
        debug:true,
        pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
 */   
    
    

  development: {
   client: 'pg',
    connection: {
      host: '127.0.0.1',
      database: 'flibuy_development',
      user:     'postgres',
      password: 'root'
    },
    debug:true,
   acquireConnectionTimeout: 10000,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
    
    

  staging: {
    client: 'postgresql',
    connection: {
      database: 'flibuy_development',
      user:     'postgres',
      password: 'root'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
