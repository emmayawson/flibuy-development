
exports.up = function(knex, Promise) {
  return knex.schema.createTableIfNotExists('purchase_confirmation', function (table) {
    table.increments().primary();
    table.integer('customer_id').notNullable().references('id').inTable('users') ;
    table.integer('auction_id').notNullable().references('id').inTable('product_auctions') ;
    table.string('control_code',50).nullable();
    table.boolean('product_recieved').nullable().defaultTo(false);
    table.string('product_recieved_date',300).nullable();
    table.string('validated_by').nullable().references('id').inTable('users') ; //sellers id
    table.text('other_details', 'mediumtext ').nullable();
    table.timestamps();
 });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('purchase_confirmation');
};
