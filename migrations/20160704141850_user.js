
exports.up = function(knex, Promise) {
    //Important the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('users', function (table) {
      table.increments().primary();
        table.string('username', 100);
        table.string('email');
        table.boolean('is_seller').defaultTo(false) ;
        table.string('password');
        table.string('provider', 20).defaultTo('password'); //password, facebook, twitter, google+
        table.integer('provider_id');
        table.timestamps();

    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('users')  
};
