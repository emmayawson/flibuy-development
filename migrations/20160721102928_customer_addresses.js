exports.up = function(knex, Promise) {
      //Important  put the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('customer_addresses', function (table) {
      table.increments().primary();
     table.integer('customer_id').notNullable().references('id').inTable('users') ;
     table.string('recipient_name',300).nullable();
     table.string('line_one',300).nullable();
     table.string('line_two',300).nullable();
     table.string('line_three',300).nullable();
     table.string('city', 150).nullable();
        table.string('post_code', 50).nullable();
        table.string('state', 150).nullable();
        table.string('country', 150).nullable();
        table.text('other_details', 'mediumtext ').nullable();
        table.boolean('deleted').defaultTo(false); //Hide from user if deleted
        table.timestamps();
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('customer_addresses');
};
