
exports.up = function(knex, Promise) {
      //Important  put the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('product_ratings', function (table) {
      table.increments().primary();
        table.integer('product_id').notNullable().references('id').inTable('products') ;
        table.integer('rated_by').notNullable().references('id').inTable('users');
        table.integer('rating').nullable();
        table.text('comments', 'mediumtext ');
        table.boolean('hide').defaultTo(false); //Pervent the display of wrong/bad questions
        table.timestamps();

    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('product_ratings');
};