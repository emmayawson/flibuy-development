
exports.up = function(knex, Promise) {
      //Important  put the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('product_bids', function (table) {
      table.increments().primary();
        table.integer('product_id').notNullable().references('id').inTable('products') ;
        table.integer('product_auctions_id').notNullable().references('id').inTable('product_auctions') ;
        table.integer('customer_id').notNullable().references('id').inTable('users') ;
        table.integer('quantity_purchased');
        table.boolean('payment_card_auth_successful').defaultTo(false);
        table.boolean('payment_card_capture_successful').defaultTo(false);
        table.dateTime('auth_successful_date').nullable();
        table.dateTime('catpure_successful_date').nullable();
        table.timestamps();
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('product_bids');
};
