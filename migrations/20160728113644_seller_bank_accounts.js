
exports.up = function(knex, Promise) {
      //Important  put the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('bank_accounts', function (table) {
     table.increments().primary();
     table.integer('seller_id').notNullable().references('id').inTable('users') ;
     table.string('account_number',100).nullable();
     table.string('account_name',100).nullable();
     table.string('bank_name',200).nullable();
     table.boolean('verified').defaultsTo(false);
     table.dateTime('verified_at').nullable();
     table.integer('verified_by').nullable(); //User must me an  admin
     table.text('admin_remarks','mediumtext').nullable(); //User must me an  admin
     table.text('seller_remarks','mediumtext').nullable();
     table.timestamps();
   });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('bank_accounts');
};
