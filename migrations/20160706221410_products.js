
exports.up = function(knex, Promise) {
    //Important the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('products', function (table) {
      table.increments().primary();
        table.integer('seller_id').notNullable().references('id').inTable('users') ;
        table.string('name');
        table.boolean('is_new').defaultTo(true) ;
        table.boolean('unlimited').defaultTo(false) ;
        table.boolean('deleted').defaultTo(false) ;
        table.string('slug');
        table.string('category', 50); //password, facebook, twitter, google+
        table.string('sub_category',50);
        table.text('short_description', 'mediumtext');
        table.text('long_description', 'longtext');
        table.string('ref').nullable();
        table.string('sku').nullable(); //Stock keeping unit 
        table.string('type').nullable(); //Digital, service, product, 
        table.string('manufacturer').nullable();
        table.timestamps();

    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('products')  
};

