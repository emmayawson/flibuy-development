
exports.up = function(knex, Promise) {
      //Important the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('product_images', function (table) {
      table.increments().primary();
        table.integer('product_id').notNullable().references('id').inTable('products') ;
        table.json('url_one').nullable();
        table.string('url_one_public_id', 3000).nullable(); // For deleting the image
        table.json('url_two').nullable();
        table.string('url_two_public_id',3000).nullable(); 
        table.json('url_three').nullable(); 
        table.string('url_three_public_id',2000).nullable(); 
        table.json('url_four').nullable(); 
        table.string('url_four_public_id',3000).nullable(); 
        table.timestamps();

    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('product_images');
};
