exports.up = function(knex, Promise) {
      //Important  put the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('seller_ratings', function (table) {
      table.increments().primary();
        table.integer('seller_id').notNullable().references('id').inTable('users') ;
        table.integer('rated_by').notNullable().references('id').inTable('users');
        table.integer('rating').nullable();
        table.text('comments', 'mediumtext ');
        table.boolean('hide').defaultTo(false); //Pervent the display of wrong/bad questions
        table.timestamps();

    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('seller_ratings');
};