
exports.up = function(knex, Promise) {
      //Important  put the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('customer_payment_info', function (table) {
      table.increments().primary();
     table.integer('customer_id').notNullable().references('id').inTable('users') ;
     table.string('stripe_customer_id',50).nullable();
     table.string('stripe_card_id',100).nullable();
     table.string('card_brand',100).nullable();
     table.string('card_last4',100).nullable();
     table.string('card_exp_month',100).nullable();
     table.string('card_exp_year',100).nullable();
     table.string('card_cvc_check',50).nullable();
     table.string('card_country_code',50).nullable();
     table.string('client_ip',50).nullable();
     table.timestamps();
   });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('customer_payment_info');
};
