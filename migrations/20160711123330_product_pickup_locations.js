//This table will contain a list of addresses where customers a
//will go to pickup the products they have purchased
exports.up = function(knex, Promise) {
      //Important  put the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('product_pickup_locations', function (table) {
      table.increments().primary();
      table.integer('seller_id').notNullable().references('id').inTable('users') ;
      table.string('address_type',100) ;
      table.string('line_one',300).nullable();
      table.string('line_two',300).nullable();
      table.string('line_three',300).nullable();
       table.string('city', 150).nullable();
        table.string('post_code', 50).nullable();
        table.string('state', 150).nullable();
        table.string('country', 150).nullable();
        table.text('other_details', 'mediumtext ');
        table.boolean('deleted').defaultTo(false); //Hide from user if deleted
        table.timestamps();

    })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('product_pickup_locations');
};
