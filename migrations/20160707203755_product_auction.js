
exports.up = function(knex, Promise) {
      //Important the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('product_auctions', function (table) {
      table.increments().primary();
        table.integer('product_id').notNullable().references('id').inTable('products') ;
        table.float('price');
        table.float('final_price');
        table.integer('discount_quantity_one').defaultTo(0) ;
        table.integer('discount_quantity_two').defaultTo(0) ;
        table.integer('discount_quantity_three').defaultTo(0) ;
        table.integer('discount_quantity_four').defaultTo(0) ;
        table.integer('discount_price_one').defaultTo(0) ;
        table.integer('discount_price_two').defaultTo(0) ;
        table.integer('discount_price_three').defaultTo(0) ;
        table.integer('discount_price_four').defaultTo(0) ;
        table.integer('limit_per_purchase').nullable() ;
        table.integer('total_quantity');
        table.boolean('visible_in_store').defaultTo(false) ; /*table.integer('pickup_address').nullable().references('id').inTable('product_pickup_locations');*/
         table.integer('pickup_address').nullable();
        table.dateTime('start_date');
        table.dateTime('end_date');
        table.boolean('completed').defaultTo(false); //completed or not completed
        table.boolean('canceled').defaultTo(false); //When the seller decide con cancel the sale
        table.dateTime('canceled_date').nullable(); //When the seller decide con cancel the sale
        table.boolean('is_local_sale').defaultTo(false) ;//products that the user will have to go and pickup in store personally
        table.timestamps();
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('product_auctions');
};
