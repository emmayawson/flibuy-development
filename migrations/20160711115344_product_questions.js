
exports.up = function(knex, Promise) {
      //Important  put the RETURN keyword, else it will not work
     return knex.schema.createTableIfNotExists('product_questions', function (table) {
      table.increments().primary();
        table.integer('product_id').notNullable().references('id').inTable('products') ;
        table.integer('asked_by').notNullable().references('id').inTable('users');
        table.integer('responded_by').nullable().references('id').inTable('users');
        table.text('question', 'mediumtext ').defaultTo(null);
        table.text('response', 'mediumtext ').defaultTo(null);
        table.integer('votes').nullable();
        table.boolean('hide').defaultTo(false); //Pervent the display of wrong/bad questions
        table.dateTime('responded_at').nullable();
        table.timestamps();
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('product_questions');
};
