var knex = require('../database');
var bookshelf = require('bookshelf')(knex);

//console.log(knex);



var User = bookshelf.Model.extend({
  tableName: 'users',
  hasTimestamps: true,
});

module.exports = User;