// Dependencies
var express = require('express');
var app = express();
var router = express.Router();
var bcrypt = require('bcrypt-nodejs');
var  session     = require('express-session');
var  trim     = require('trim');
//Models
//var User = require('../../../models/user');
var knex = require('../../../database');
//console.log(knex);



   router.post('/login-business',function(req, res) {
        console.log(req.body);
    if(req.body.password && req.body.email){
      // var hashedPass = passwordHash.verify(req.body.password, hashedPassword); 
        var pass = trim(req.body.password);
        console.log(pass);
     var query =    knex.from('users')
           .where({'email': req.body.email})
           .first();
        return query.then(function(results) {
           var  user = {};
             if(bcrypt.compareSync(pass, results.password)){
                  
                 user.id        = results.id;
                 user.username  = results.username;
                 user.email     =  results.email;
                 user.is_seller = results.is_seller;
                 
                 req.session.user = user;
                if (results.is_seller == true){
                   return  res.redirect('/business-dashboard'); 
                }else{
                   return  res.redirect('/customer-dashboard'); 
                }
                 
              }else{
                 res.json({Error:'Email/contraseña no valido'});
              }
           
          })
           .catch(function(err) {
          console.log( err);
            return res.json({error:err.message}) ;
        });
        

    }else{
         return  res.redirect('/login-business');
    }
    
    });

router.get('/login-business',function(req, res) {
       return res.render('business/login/index')
    });


// Return router
module.exports = router;