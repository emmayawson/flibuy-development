var config        = require('../../../modules/config-loader')(process.env.APP_ENV);
var express = require('express');
var app     = express();
var router  = express.Router();
var path    = require('path');
var fs      = require('fs');
var randomstring = require("randomstring");
var knex = require('../../../database');


var multer      = require('multer');
//var upload = multer({ dest: 'uploads/' });

//Multer setup
var storage        = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
       // cb(null, file.fieldname + '-' + Date.now())
        cb(null, Math.random().toString(36) + path.extname(file.originalname));
    }
});
var upload         = multer({ storage: storage }).array('image',2);

var cloudinary  = require('cloudinary');
cloudinary.config({
    cloud_name: config.CLOUDINARY_CLOUD_NAME,
    api_key: config.CLOUDINARY_API_KEY,
    api_secret: config.CLOUDINARY_API_SECRET
});




     router.post('/product-images/create',function(req, res) {

         //Create anew filename

    var newFileName = randomstring.generate({
        length: 20
    });

             upload(req,res,function(err) {
         console.log(req.body);
         console.log(req.files);
         console.log(req.files[0].path);
                 if(req.files.length){


                         cloudinary.uploader.upload(req.files[0].path, function(image){
        // "eager" parameter accepts a hash (or just a single item). You can pass
        // named transformations or transformation parameters as we do here.

        console.log("** Eager Transformations");
        //console.log(image);
        //if (err){ console.warn(err);}
        //console.log(" * " + image);
        //console.log("* "+image.public_id);
        //console.log("* "+image.eager[0].secure_url);
        waitForAllUploads(image);

    },  { public_id : "product_images/" + newFileName, resource_types: 'image'});

     function waitForAllUploads( image){
       console.log(image);

         return knex('product_images')
            .insert({product_id:req.body.product_id,
                     url_one: JSON.stringify(image), url_one_public_id:image.public_id })
            .then(function(response){

             //delete image from file system
               var filePath = req.files[0].path ;
                     fs.unlinkSync(filePath);
             console.log(response);
             return res.json(response);
         })
         .catch(function(err){
              console.log(err);
              var filePath = req.files[0].path ;
                     fs.unlinkSync(filePath);
            return  res.json(err);
         });

  /*
         //TODO Delete image from DB befor update
         User.findOne({_id:req.user._id}, function(err, user){

             if(!err){
                 cloudinary.api.delete_resources([user.avatar_cloudinary_id],
                     function(result){
                      //   console.log(result);
                     }, { keep_original: false });
             }else{

             }
         });
*/

     }


                 }
                  //return res.end('Saved image!');
        if(err) {
            return res.end("Error uploading file.");
        }
    });


         if(req.body.product_id){

         }


    });


router.post('/product-images/update',function(req, res) {


});

router.get('/product-images/:id/update',function(req, res) {


});

router.post('/product-images/:id/delete',function(req, res) {


});


module.exports = router;
