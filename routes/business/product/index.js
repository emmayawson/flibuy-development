// Dependencies
var express = require('express');
var app = express();
var router = express.Router();
var slug = require('slug');

//Databse
var knex = require('../../../database');

//Models
//var User = require('../../../models/user');



  router.post('/products/create',function(req, res) {
      console.log(req.body);
      var is_new;
     var titleSlug = slug(req.body.name);
      if(req.body.is_new == 'No'){
          is_new = false;
      }else{
            is_new = true;
      }
      //TODO Add the ID of the newly created product to thge slug
      knex('products')
      .insert({
               seller_id: req.session.user.id,
               name: req.body.name,
              slug: titleSlug,
              category:req.body.category,
              sub_category:  req.body.sub_category,
              type:  req.body.type,
              is_new: is_new,
              ref: req.body.ref,
              sku: req.body.sku,
              manufacturer: req.body.manufacaturer,
              short_description: req.body.short_description,
              long_description: req.body.long_description,
             })
      .returning('id')
      .then(function(id){

          //console.log(id[0]);
          var productId = id[0];
          titleSlug = titleSlug+'-'+productId;
          //console.log(titleSlug);
           knex('products')
          .where({id:productId})
           .update({slug: titleSlug})
           .then(function(data){
               //console.log(data);
           })
           .catch(function(err){
               //console.log(err);
            });
        return   res.json({message: '/products'});
      })
      .catch(function(err){
           console.log(err);
      });


    });

router.get('/products/create',function(req, res) {
        res.render('business/product/create', {});
    });

router.get('/products/show/:id',function(req, res) {
        res.render('business/product/show', {});
    });


router.get('/products',function(req, res) {

    if(req.session.user){
        knex('products')
            .where({seller_id: req.session.user.id})
                .then(function(results){
                    console.log(results);
                res.render('business/product/index', {products: results});
                })
                .catch(function(err){
                    console.log(err);
                });
    }

    });


// Return router
module.exports = router;
