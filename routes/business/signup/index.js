// Dependencies
var express = require('express');
var app = express();
var router = express.Router();
var bcrypt = require('bcrypt-nodejs');

//Models
var User = require('../../../models/user');



  router.post('/signup-business',function(req, res) {
        console.log(req.body);
    if(req.body.password && req.body.name && req.body.email){
       //var hashedPass = passHash.generate(req.body.password);

        //Check if email exists


        var user = new User();
        user.set('username', req.body.name );
        user.set('email', req.body.email );
        user.set('password', bcrypt.hashSync(req.body.password));
        user.set('is_seller', true);
        user.set('provider','password');
        user.set('provider_id', null);
        user.save()
            .then(function(newUser){
            console.log(newUser);
            //Send conformation request email to user
            return res.redirect('/login-business');
        });

    }else{
        res.json({error: 'Hay campos obligatorios que estan vacios'});
    }

    });

router.get('/signup-business',function(req, res) {
        res.render('business/signup/index', {
            pageTitle: 'Crear cuenta'
        });
    });



// Return router
module.exports = router;
