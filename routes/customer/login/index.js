// Dependencies
var express = require('express');
var app = express();
var router = express.Router();
var  session     = require('express-session');
var bcrypt = require('bcrypt-nodejs');
var trim = require('trim');
//Models
//var User = require('../../../models/user');
//DB
var knex = require('../../../database');




  router.post('/login-customer',function(req, res) {
        console.log(req.body);
    if(req.body.password && req.body.email){
     var query =    knex.from('users')
           .where({'email': trim(req.body.email)})
           .first();
        return query.then(function(model) {
             console.log(model);
             
            
             if(bcrypt.compareSync(req.body.password,  model.password)){
                
                    delete model.password;
                    delete model.created_at;
                    delete model.updated_at;
                 
                  req.session.user = model;
                  console.log(req.session.user );
                 return  res.json({redirectTo:'/customer-dashboard'});
              }else{
                 return res.json({error:' Su Email/contraseña no es válido'});
              }
           
          })
           .catch(function(err) {
           console.log( err);
            return res.json({error: err.message}) ;
        });
        

    }else{
        res.json({error: 'Introduzca su email/contraseña'});
    }
    
    });

router.get('/login-customer',function(req, res) {
    //return res.json(req.body);
       return  res.render('customer/login/index', {})
    });


// Return router
module.exports = router;