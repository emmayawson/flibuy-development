// Dependencies

//Models
//var User = require('../../../models/user');
var knex = require('../../../database');
//console.log(knex);


router.post('/bid', function(req, res){
    console.log(req.body);
    
    //Check for user session
      
    // Check if the user is not already buying the product
    
    //CHeck for payment card

    knex('product_bids')
    .insert({
        customer_id: req.session.user.id,
        product_id:req.body.product_id,
        product_auctions_id:req.body.product_auction_id,
        quantity_purchased:req.body.quantity_purchased,
        created_at: knex.fn.now()
    }).then(function(results){
        console.log(results);
    }).catch(function(error){
       return console.log(error);
    });
    //Get the pro
    return res.json({message:'OK'});
});

router.post('/cancel-bid', function(req, res){
     console.log(req.body);
});

// Return router
module.exports = router; 