// Dependencies
var express = require('express');
var app = express();
var router = express.Router();
var bcrypt = require('bcrypt-nodejs');
var trim = require('trim');

//DB
var knex = require('../../../database');
//Models
var User = require('../../../models/user');


router.get('/signup-customer', function(req, res) {
    res.render('customer/signup/index', {
      pageTitle: 'Crear cuenta de cliente'
    });
   
});

  router.post('/signup-customer',function(req, res) {
        console.log(req.body);
    if(req.body.password && req.body.name && req.body.email){
       //var hashedPass = passHash.generate(req.body.password); 
        
        //Check if email exists
        
        
        var user = new User();
        user.set('username', req.body.name );
        user.set('email', trim(req.body.email) );
        user.set('password', bcrypt.hashSync(trim(req.body.password)) );
        user.set('is_seller', false);
        user.set('provider','password');
        user.set('provider_id', null);
        user.set('created_at', knex.fn.now());
        user.save()
            .then(function(newUser){
                console.log(newUser);
                //Send conformation request email to user
                return res.json({redirectTo:'/login-customer', message:'Cuenta creada'});
            })
            .catch(function(err){
                console.log(error);
            return res.json({ message:err});
        });

    }else{
      return res.json({ error: 'Algunos campos obligatorios estan vacios'});  
    }
    
    });





// Return router
module.exports = router;