
var Promise = require('bluebird');
//DB
var knex = require('../../database');
   

module.exports = function(app){
    var  product = {} ;  
 
    


product.index =  app.get('/', function(req, res) {
    return res.render('home/index', {
        pageTitle: 'Proximo lanzamiento'
    });
   
});


product.home =  app.get('/shop', function(req, res) {
    
var query =  knex('products')
        .innerJoin('product_auctions', 'products.id', 'product_auctions.product_id')
        .where({'product_auctions.visible_in_store': true});
       return  query.then(function(results){
        return res.render('home/store', {
                pageTitle: 'Inicio',
                     products: results,
                });
    })
    .catch(function(error){
       return console.log(error);
    });
   
});


product.getOne =  app.get('/shop/products/show/:slug', function(req, res) {
    //Get single product by ID or BY UNIQUE SLUG
    console.log(req.params.slug);
    var productPromise = knex('products')
                            .where({slug:req.params.slug})
                            .innerJoin('product_auctions', 'products.id', 'product_auctions.product_id')
                            .then();
   /*
    var productAuctionPromise = knex('products')
                            .where({slug:req.params.slug})
                            .innerJoin('product_auctions', 'products.id', 'product_auctions.product_id')
                            .select('product_auctions.id')
                            .then();
    **/
    Promise.all([productPromise ])
    .then(function(results){
             //  console.log(results); 
               console.log(results); 
        var product = results[0][0]; 
        //var productAuctionId = results[1][0].id;
        var productAuctionId = 1;
       // console.log(productAuctionId);
        
            return res.render('home/product-details', {
        pageTitle:product.slug,
             product: product,
             productAuctionId: productAuctionId
        });
    })
    .catch(function(error){
       return console.log(error);
    });
    

   
});
   
    
    return product;
};
